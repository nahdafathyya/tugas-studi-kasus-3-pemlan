package studiKasus3;

import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList mahasiswa = new ArrayList();
        Scanner in = new Scanner(System.in);

        boolean nextBio = true;
        while (nextBio){
            System.out.print("Masukkan NIM : ");
            String nim = in.nextLine();
            System.out.print("Masukkan nama : ");
            String nama = in.nextLine();

            Mahasiswa mhs = new Mahasiswa();
            mhs.setNim(nim);
            mhs.setNama(nama);

            ArrayList khs = new ArrayList();
            boolean nextKHS = true;
            while (nextKHS){
                System.out.print("Masukkan kode : ");
                String kode = in.nextLine();
                System.out.print("Masukkan nama MK : ");
                String namaMK = in.nextLine();
                System.out.print("Masukkan nilai angka : ");
                String nilaiAngka = in.nextLine();

                MataKuliah mk = new MataKuliah();
                mk.setKode(kode);
                mk.setNamaMK(namaMK);
                mk.setNilaiAngka(nilaiAngka);
                khs.add(mk);

                System.out.print("tambah khs? (y/n) : ");
                String tambahKHS = in.nextLine();
                if (tambahKHS.equals("n")){
                    nextKHS = false;
                }
            }
            mhs.setKhs(khs);
            mahasiswa.add(mhs);
            System.out.print("tambah bio? (y/n) : ");
            String tambahBio = in.nextLine();
            if (tambahBio.equals("n")) {
                nextBio = false;
            }
        }
        System.out.println("===========================");
        for (int i = 0; i < mahasiswa.size(); i++){
            Mahasiswa mhs = (Mahasiswa) mahasiswa.get(i);
            System.out.println(mhs.getNim() + " | " + mhs.getNama() + " | ");
            ArrayList khs = mhs.getKhs();
            for (int j = 0; j < khs.size(); j++){
                MataKuliah k = (MataKuliah) khs.get(j);
                String nilaiHuruf = k.konversiNilai();
                System.out.println(k.getKode() + " | " + k.getNamaMK() + " | " + nilaiHuruf);
            }
        }
    }
}
