package studiKasus3;

public class MataKuliah {
    private String kode;
    private String namaMK;
    private String nilaiAngka;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNamaMK() {
        return namaMK;
    }

    public void setNamaMK(String namaMK) {
        this.namaMK = namaMK;
    }

    public String getNilaiAngka() {
        return nilaiAngka;
    }

    public void setNilaiAngka(String nilaiAngka) {
        this.nilaiAngka = nilaiAngka;
    }

    public String konversiNilai() {
        int nilai = Integer.parseInt(nilaiAngka);
        String n;
        if (nilai > 80) {
            n = "A";
        } else if (nilai <= 80 && nilai > 75) {
            n = "B+";
        } else if (nilai <= 75 && nilai > 69) {
            n = "B";
        } else if (nilai <= 69 && nilai > 60) {
            n = "C+";
        } else if (nilai <= 60 && nilai > 55) {
            n = "C";
        } else if (nilai <= 55 && nilai > 50) {
            n = "D+";
        } else if (nilai <= 50 && nilai > 44) {
            n = "D";
        } else {
            n = "E";
        }
        return n;
    }

}
